
jQuery(document).ready(function(){

  let $ = jQuery;

  let api = 'https://sheets.googleapis.com/v4/spreadsheets/1jFewbUjSYfvJZWHdZDrw91uR5hNz1eYLKtfKTi6x6IY/values/Sheet1?valueRenderOption=FORMATTED_VALUE&key=AIzaSyDEoSyZpJfASo6kdSn55k8Wa6IK_iw-4sQ'


  let opt1 = $('.gf_dropdown_1');
  let opt2 = $('.gf_dropdown_2');
  let opt3 = $('.gf_dropdown_3');

 $.get(api, function (data) {
  let jField = data.values
  let array = jField,
  keys = array.shift(),
  transformArr = array.map(v => Object.assign(...keys.map((k, i) => ( { [k]: v[i] } ))));

  let str = "<option>Select Value</option>"; 

  let arr = [];

  transformArr.forEach(element => {
  arr.push(element.location);
  });

  let uniqueLocations = Array.from(new Set(arr))

  uniqueLocations.forEach(function(loc) {
  str += `<option value="${loc}">${loc}</option>`
  })

  opt3.find( ".gfield_select" ).html("")
  opt1.find( ".gfield_select" ).html(str)

 })

  opt1.change(function(){
    let el = $(this).find(":selected").val();
    $.get(api, function(data){
      let jField = data.values
      let array = jField,
      keys = array.shift(),
      transformArr = array.map(v => Object.assign(...keys.map((k, i) => ( { [k]: v[i] } ))));

      let dates = transformArr.filter(function (data) {
        return data.location == el
      })
      
      let str = "<option>Select Value</option>";

      dates.forEach(function(element){
        str += `<option value="${element.date}">${element.date}</option>`
        console.log(element)
      });

      opt2.find( ".gfield_select" ).html(str)

      })
  })
})