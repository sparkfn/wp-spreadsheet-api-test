<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!QQRwX]ANbESn C|FU#NNC`,FLxyX[yQzf,dtxo8Zj8*ytO/}a<1MqF;i.<Ntlen');
define('SECURE_AUTH_KEY',  '@A1t-XZ#><sUs<#Nod^7{A?:x-g9|AdW!RdBV4W(`0q |t7Uv1Rm67A[*8LEcwE4');
define('LOGGED_IN_KEY',    'z~:&z}VH18|$q`Q/ZpJ?GRssb5dNVp{j)K3TJ0zGIZH!t!n9q?]}p(UmPt*&Z>HN');
define('NONCE_KEY',        'fu4:v(ChjeQ%lG8.pFx5_I#MQ0.qVB5J8t=(tzxV]UP=9s%pf(1/hf%=X9+56o#]');
define('AUTH_SALT',        '+:2N,kOq+2NKB+nZ_xukme8R-~]><RG2^i+v2a?@7)|n5`1|qvUtDbaw,8*|;E7=');
define('SECURE_AUTH_SALT', 'EWKZqc9Z96xZ:IXME_0j UmRqU+mepep&qpHR|kdq64:*aW9Do/-O6:QroM*>otn');
define('LOGGED_IN_SALT',   'vSU18RTS)I%WiqH0|rCJ_$hxr7RN#7j/}i53Ez_+&PN3C[$46JxUi]@zD{TeTYKE');
define('NONCE_SALT',       'h7f6w0kYvG~DOJCycu^a20Q!elFog|xV1k@HUY#!n7*B<2alfE8H<dJSV]QBo8!a');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
